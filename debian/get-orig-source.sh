#!/bin/sh

set -ex

name="$(debian/make-upstream-tarball.sh print-name)"

debian/make-upstream-tarball.sh
mv "${name}"*.tar.xz ..
mk-origtargz "../${name}.tar.xz"
